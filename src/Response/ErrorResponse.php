<?php

namespace InstagramAPI\Response;

use InstagramAPI\Response;

/**
 * LoginResponse.
 *
 * @method string getMessage()
 * @method string getStatus()
 * @method string getError_type()
 * @method Model\CheckpointChallengeRequired getCheckpointChallengeRequired()
 */
class ErrorResponse extends Response
{
    const JSON_PROPERTY_MAP = [
        'message'    => 'string',
        'challenge'  => 'Model\CheckpointChallengeRequired',
        'status'     => 'string',
        'error_type' => 'string'
    ];
}
